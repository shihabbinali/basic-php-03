<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Office {
    
    function createDocument() {
        echo "creating doc";
        
        return $this;
    }
    
    function writeText() {
        echo "writting text";
        
        return $this;
    }
    
    function saveDocument() {
        echo "saving doc";
        
        return $this;
    }
    
    function printDocument() {
        echo "printing doc";
        
        return $this;
    }
    
    
}

$word = new Office();

$word->createDocument();
$word->writeText();
$word->saveDocument();
$word->printDocument();

$word->createDocument()->writeText()->saveDocument()->printDocument();
