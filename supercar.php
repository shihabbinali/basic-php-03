<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class Car {
    
    public function setSpeed() {
        
    }
    
    public function getSpeed() {
        
    }
    
    public function setEngineCapacity() {
        
    }
    
    public function getEngineCapacity() {
        
    }
    
    public function setNumberDoors() {
        
    }
    
    public abstract function getNumberDoors();
}

class Nano extends Car {
    
    public function getNumberDoors() {
        return 2;
    }
    
}



class Ferrari extends Car {
    public function getNumberDoors() {
        return 2;
    }
}

class BMW extends Car {
    public function getNumberDoors() {
        return 4;
    }
}

$nano = new Nano();
$nano->getNumberDoors();