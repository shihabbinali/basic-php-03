<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface Car {
    
    public function getWheelSize();
    public function getSpeedLimit();
    public function getEngineCC();
    
}

interface Engine {
    public function getManufacturer();
}

Class Myauth {

}

class Ferrari implements Car,  Engine {
    public function getWheelSize(){}
    public function getSpeedLimit() {}
    public function getEngineCC() {}
    public function getManufacturer() {echo "i am Ferrari";}
}

class BMW implements Car, Engine {
    public function getWheelSize(){}
    public function getSpeedLimit() {}
    public function getEngineCC() {}
    public function getManufacturer() {echo "i am BMW";}
}

function showCC($car) {
    echo $car->getManufacturer();
}

$a = new Myauth();

showCC($a);

showCC(new Ferrari());

showCC(new BMW());