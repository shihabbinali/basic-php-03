<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
echo "<pre>";

Class Security {
    public $securityParam;
    
    public function __construct($securityParam) {
        $this->securityParam = $securityParam;
    }
    
    
}

class Car {
    
  public $color = "";
  public $mfg   = "";
  private $model = "";
  protected $type  = "";
  public $cc    = "";
  public $security;
  
    
  
  public function __construct($color = "", $mfg = "",$model ="", $type = "", $cc = "", Security $sp) {
      $this->color = $color;
      $this->mfg = $mfg;
      $this->model = $model;
      $this->type = $type;
      $this->cc = $cc;
      
      $this->security = $sp;
  }
  
  
  public function __clone() {
      echo "I am cloning\n";
      
      foreach($this as $key => $val) {
          if(gettype($val) == "object" || gettype($val) == "array") {
              $this->{$key} = unserialize(serialize($val));
          }
      }

  }
  
  public function __destruct() {
      echo "I am destryoing everything\n";
  }
  
  function __get($name) {
      echo "Sorry we do not have $name\n";
  }
  
  function getColor() {
      return $this->color;
  }
  
  function setColor($color = "") {
      $this->color = $color;
  }
  
  public function getModel() {
      return $this->model;
  }
  
  public function setModel($model) {
      $this->model = $model;
  }
    
  public function showMyInfo() {
      //global $color;
      echo "About Me: \n Color:".$this->color."\n Manuf: ".$this->mfg."\n Model:".$this->model."\n";
  }
  
      
}

$security1 = new Security("level1 security");
$security2 = new Security("level two security");

$ferrari = new Car("red","Ferrari","FZ5000","Sports","2400", $security1);

print_r($ferrari);

$bmw = clone $ferrari;
$bmw->color = "blue";
$bmw->mfg = "British Motor Works";
$bmw->security->securityParam = "new security";
print_r($bmw);


$ferrari->color = "red";

print_r($ferrari);
print_r($bmw);