<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include "dbconf.php";

$dsn = "mysql:host=localhost;dbname=batch_three";

$pdo = new PDO($dsn, $username, $password);

if(! $pdo) {
    die (":(");    
}

$query = "select * from students where id = '".$_GET['id']."' limit 1";
$result = $pdo->query($query);

$student = $result->fetchObject();

if($student):
?>
<html>
    <head>
        <title>Hello world form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    </head>
    <body>
        <div>Edit Students</div>
        
        <form method="POST" id='form' action="save.php" enctype='multipart/form-data'>
            Name:<br />
            <input type="text" name="name" value="<?php echo $student->name?>" />
            <br />
            
            Email<br />
            <input type="text" name="email" value="<?php echo $student->email?>" />
            <br />
            
            Phone<br />
            <input type="text" name="phone" value="<?php echo $student->phone?>" />
            <br />
            
            Course<br />
            <input type="text" name="course" value="<?php echo $student->course?>" />
            <br />
            
            <input type="hidden" name="id" value="<?php echo $student->id?>" />
            
            <input type="submit" value="Save" />
            
            <input type='button' value='Ajax Submit' onclick='ajaxSubmit()' />
            
            
            
            
        </form>
        
        <div id='showMe'>
            
        </div>
        
        <script>
            function ajaxSubmit() {
                console.log('calling');
            $.ajax({
              type: "POST",
              url: "save.php",
              data: $("#form").serialize(),
              success: function(data) {
                  $("#showMe").html(data);

              }
            });

            }
            
        </script>
    </body>
</html>
<?php
endif;
?>