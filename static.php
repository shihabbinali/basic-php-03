<?php

$countA = 0;

function myPrint() {
    static $countA = 0;
    
    $countA++;
    echo "<br />I am printing ".$countA;
}

function myPrintNew() {
    static $countA = 0;
    
    $countA++;
    echo "<br />I am printing from New".$countA;
}

myPrint();
myPrintNew();
myPrint();
myPrintNew();
myPrintNew();
myPrint();
myPrint();
myPrint();
myPrint();