<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait Mysecurity {
    
    public $int = 10;
    
    public function showMyName() {
        echo "I am a trait";
    }
    
}

trait Myfiles{
    public function myFunction() {
        echo "I am from file";
    }
    
}

class Accounting {
    
    use Mysecurity,Myfiles;
    
}

$a = new Accounting();
echo $a->int;
$a->myFunction();